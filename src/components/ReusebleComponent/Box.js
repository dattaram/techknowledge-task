import React from 'react'

export default function Box(props) {
    return (
        <div style={{width:'30%'}}>
            <p className="heading">{props.heading}</p>

            <div className="box">{props.children}</div>
        </div>
    )
}
