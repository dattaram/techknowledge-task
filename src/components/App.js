import React from 'react'
import TopNavbar from './navbar/TopNavbar'
import SideNavbar from './navbar/SideNavbar'
import {BrowserRouter as Router,Switch, Route} from 'react-router-dom'
import Overview from './overview/Overview'
import Training from './resources/Training'
import Interview from './resources/Interview'
import Test from './resources/Test'
import Coaching from './resources/Coaching'
import Mentoring from './resources/Mentoring'

export default function App() {
  return (
    <Router>
      <TopNavbar />
      <SideNavbar />
      <Switch>
          <Route exact path="/" component={Overview} />
          <Route exact path='/trainings' component={Training}/>
          <Route exact path="/interview" component={Interview} />
          <Route exact path="/test" component={Test} />
          <Route exact path='/coaching' component={Coaching}/>
          <Route exact path="/mentoring" component={Mentoring} />
      </Switch>
    </Router>
  )
}
