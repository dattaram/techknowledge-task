import React from 'react'
import { MdDateRange } from "react-icons/md";
import Box from '../ReusebleComponent/Box';
import { FaUser } from "react-icons/fa";

export default function Coaching(props) {
    return (
        <Box heading="Coach Details">
            <div className="profileImg flex" style={{justifyContent:'center',color:'black'}}>
                <FaUser style={{width:'30px',height:'30px'}}/>
            </div>
            <div style={{textAlign:'center'}}>
                <p className='empName'>{props.data.title} {props.data.first_name} {props.data.last_name}</p>
                <p className='designation'>{props.data.designation}</p>
            </div>

            <div style={{marginTop:'50px'}}>
                <div className="flex" style={{marginBottom:'20px'}}>
                    <div className="coachIcon"><MdDateRange /></div>
                    <div>
                        <p className="avgText">Company</p>
                        <p className="coachInfo">{props.data.current_comapny}</p>
                    </div>
                </div>
                <div className="flex" style={{marginBottom:'20px'}}>
                    <div className="coachIcon"><MdDateRange /></div>
                    <div>
                        <p className="avgText">Experience</p>
                        <p className="coachInfo">{props.data.relevant_experience} Years</p>
                    </div>
                </div>
            </div>
        </Box>
    )
}
