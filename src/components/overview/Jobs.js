import React from 'react'
import Box from '../ReusebleComponent/Box'

export default function Jobs(props) {
    return (
        <Box heading='Eligible Jobs'>
            <p className="avgText marginBottom">
                If you complete this training you will be eligible for good job opportunities
            </p>

            {props.data.map((data,i) =>(
                <div className={props.data.length === i+1 ?'':'borderBottom'} key={data.id}>
                    <p className="job">{data.position}</p>
                </div>
            ))}
        </Box>
    )
}
