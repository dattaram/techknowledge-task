import React from 'react'
import Box from '../ReusebleComponent/Box'
import { MdHourglassEmpty } from "react-icons/md";

export default function TimeCommitment(props) {
    return (
        <Box heading="Time Commitment">
            <div className="flexSpaceBetween">
                <div className="flex" style={{width:'90%'}}>
                    <div className="icon" style={{background:'red'}}>
                        <MdHourglassEmpty />
                    </div>
                    <div>
                        <p className="avgText">Total hrs for Course</p>
                        <p className="avgTextInfo">{props.data.total_course_duration_in_hours} Hrs</p>
                    </div>
                </div>
                <div className="flex" style={{width:'90%'}}>
                    <div className="icon" style={{background:'green'}}>
                        <MdHourglassEmpty />
                    </div>
                    <div>
                        <p className="avgText">Average no. of days</p>
                        <p className="avgTextInfo">{props.data.average_days_required} Days</p>
                    </div>
                </div>
            </div>

            <div style={{textAlign:'center',margin:'30px 0'}}>
                <div className="emoji"></div>
                <p>Superb!</p>
                <p className="avgText">Number of hrs as per commitment</p>
                <p>{props.data.days_as_per_user_commitment} Days</p>
            </div>

            <div>
                <p className="weekInfo">You can change your weekend and weekdays timecommitment</p>
                <div>
                    <p className="font14">Weekday</p>
                    <div className="flexSpaceBetween">
                        <div className="slider"></div>
                        <p className="hrsBox">{props.data2.weekday_commitment} HRS</p>
                    </div>
                </div>
                <div>
                    <p className="font14">Weekend</p>
                    <div className="flexSpaceBetween">
                        <div className="slider"></div>
                        <p className="hrsBox">{props.data2.weekend_commitment} HRS</p>
                    </div>
                </div>
            </div>
        </Box>
    )
}
