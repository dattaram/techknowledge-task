import React from 'react'
import Box from '../ReusebleComponent/Box'

export default function Salary(props) {
    return (
        <Box heading='Salary Hike'>
            <p className="avgText marginBottom">Expected Salary Hike</p>
            <p className="salaryPer">{props.data.expected_salary_hike}%</p>
        </Box>
    )
}
