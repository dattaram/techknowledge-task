import React, { useEffect, useState } from 'react'
import CEPOverview from './CEPOverview'
import Data from '../../cep_test.json'
import TimeCommitment from './TimeCommitment'
import Skills from './Skills'
import CEPStructure from './CEPStructure'
import Coaching from './Coaching'
import Jobs from './Jobs'
import Salary from './Salary'
import CEPOverviewchartjs from './CEPOverviewchartjs'

export default function Overview(props) {
    const [data,setData]=useState({})
    const [timeCommitment,setTimeCommitment]=useState({})
    const [userTime,setUserTime]=useState({})
    const [skills,setSkills]=useState([])
    const [coach,setCoach]=useState({})
    const [jobs,setJobs]=useState([])
    const [cep_plan,setCEP_plan]=useState([])
    const [graphData,setGraphData]=useState([])

    useEffect(()=>{
        setData(Data)
        setTimeCommitment(Data.time_commitment)
        setUserTime(Data.time_commitment.users_time_commitment)
        setSkills(Data.skills_covered)
        setCoach(Data.coach_details)
        setJobs(Data.eligible_jobs)
        setCEP_plan(Data.cep_plan)
        setGraphData(Data.graph_data)
    },[])

    console.log(data,skills)
    return (
        <div className="page">
            <CEPOverview 
                data={graphData}
            />
            {/* <CEPOverviewchartjs
                data={graphData}
            /> */}

            <div className="flexSpaceBetween marginBottom">
                <TimeCommitment 
                    data={timeCommitment}
                    data2={userTime}
                />
                <Skills 
                    data={skills}
                />
                <CEPStructure 
                    data={cep_plan}
                />
            </div>
            
            <div className="flexSpaceBetween marginBottom">
                <Coaching 
                    data={coach}
                />
                <Jobs 
                    data={jobs}
                />
                <Salary 
                    data={data}
                />
            </div>

            <div>
                <p className="heading">Achievemnets after course completion</p>

                <div className="box2">
                    <div className="flex">
                        <div className="flex" style={{marginRight:'50px'}}>
                            <div className="icon" style={{background:'red'}}></div>
                            <p className="achivement">Training completion Certificate</p>
                        </div>

                        <div className='flex'>
                            <div className="icon" style={{background:'green'}}></div>
                            <p className="achivement">Job Placement</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
