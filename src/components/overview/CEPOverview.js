import React, { useEffect, useState } from 'react'
// import {Chart} from 'chart.js'
import {Chart} from 'react-google-charts'

export default function CEPOverview(props) {
    // const new=[1,2,2,3,4,5,5]
    const resource_name=Array.from(new Set(props.data.map(data=>data.resource_name)))
    const colors=['rgb(32, 65, 149)','rgb(165, 39, 20)','rgb(242, 166, 0)','rgb(15, 157, 88)','rgb(171, 71, 188)','rgb(0, 172, 193)']
    const array=[[{ type: 'string', label: 'Task ID' },
    { type: 'string', label: 'Task Name' },
    { type: 'string', label: 'Resource' },
    { type: 'date', label: 'Start Date' },
    { type: 'date', label: 'End Date' },
    { type: 'number', label: 'Duration' },
    { type: 'number', label: 'Percent Complete' },
    { type: 'string', label: 'Dependencies' }]]
    props.data.forEach((data,i)=>(array.push([i,i,data.resource_name,new Date(data.start_date),new Date(data.end_date),null,data.progress,null])))
    console.log(array)
    // const data=props.data.map((data,i)=>[i,i,null,new Date(data.start_date),new Date(data.end_date),null,data.progress,null])
    
    // const d=[]
    // for(let i=0;i<12;i++){
    //   console.log(data[data.length-i])
    //   var newData=data[data.length-i]
    //   d.push(newData)
    // }
    // const columns = [
    //   { type: "string", label: "Task ID" },
    //   { type: "string", label: "Task Name" },
    //   { type: "date", label: "Start Date" },
    //   { type: "date", label: "End Date" },
    //   { type: "number", label: "Duration" },
    //   { type: "number", label: "Percent Complete" },
    //   { type: "string", label: "Dependencies" }
    // ];

    // const [data,setData]=useState([])
    // function onAddRow() {
    //   props.data.forEach((data2,i)=>(
    //     data.push([
    //       i,i,data2.resource_name,new Date(data.start_date),new Date(data2.end_date),null,data.progress,null
    //     ])
    //   ))
    //   setData(data)
    //   console.log(data)
    // }
  
    // useEffect(()=>{
    //   onAddRow()
    //   console.log(data)
    // },[props.data])
    // console.log(data)
    return (
        <div className="marginBottom">
            <p className="heading">CEP Overview</p>
            <div className='box2 '>
                
                {/* <canvas id="canvas" style={{height:'300px',width:'100%'}}></canvas> */}
                <Chart
                  width={'100%'}
                  height={'400px'}
                  chartType="Gantt"
                  data={array}
  
  options={{
    // height: 400,
    gantt: {
      trackHeight: 30,
    },
    vAxis:{
      direction:'1'
    },
    hAxis:{
      direction:'-1'
    },
  }}
  rootProps={{ 'data-testid': '2' }}
/>
                <div className="resourceNameMain flexSpaceBetween">
                    {resource_name.map((data,i) =>{
                        return(
                        <div className="flex" key={i}>
                            <div className="skillBox" style={{background:colors[i]}}></div>
                            <p>{data}</p>
                        </div>
                    )})}
                </div>
            </div>
        </div>
    )
}
