import React, { useState } from 'react'
import Box from '../ReusebleComponent/Box'

export default function CEPStructure(props) {
    const mentoring_sessions_count=props.data.reduce((prev,next)=>prev+next.cep_resources.mentoring_sessions.count,0)
    const coaching_sessions_count=props.data.reduce((prev,next)=>prev+next.cep_resources.coaching_sessions.count,0)
    const interviews_count=props.data.reduce((prev,next)=>prev+next.cep_resources.interviews.count,0)
    const tests_count=props.data.reduce((prev,next)=>prev+next.cep_resources.tests.count,0)
    const trainings_count=props.data.reduce((prev,next)=>prev+next.cep_resources.trainings.count,0)

    const [CEPStructure]=useState([
        {id:1,name:'Mentoring Sessions'},
        {id:2,name:'Coaching Sessions'},
        {id:3,name:'Interviews'},
        {id:4,name:'Tests'},
        {id:5,name:'Trainings'}
    ])
    const count=[mentoring_sessions_count,coaching_sessions_count,interviews_count,tests_count,trainings_count]
    const colors=['#FF5733','#FFA833','#52FF33','#33C1FF','#FFc1cc']
    return (
        <Box heading="CEP Structure">
            <p className="avgText">
                Here is detail career enhancement plan for you
            </p>

            {CEPStructure.map((data,i)=>(
                <div className={CEPStructure.length===i+1?'':'borderBottom'} key={data.id}>
                    <div className="flexSpaceBetween" style={{padding:'10px 0'}}>
                        <div className="flex">
                            <div className="icon" style={{background:colors[i]}}></div>
                            <p className="cepStructureName">{data.name}</p>
                        </div>
                        <p className="count">{count[i]}</p>
                    </div>
                </div> 
            ))}
            <button className="CEPButton">career enhancement plan</button>
        </Box>
    )
}
