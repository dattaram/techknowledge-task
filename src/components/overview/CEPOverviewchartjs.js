import React, { useEffect, useState } from 'react'
import {Chart} from 'chart.js'

export default function CEPOverviewchartjs(props) {
    const resource_name=Array.from(new Set(props.data.map(data=>data.resource_name)))
    const colors=['rgb(32, 65, 149)','rgb(165, 39, 20)','rgb(242, 166, 0)','rgb(15, 157, 88)','rgb(171, 71, 188)','rgb(0, 172, 193)']


    useEffect(()=>{
        canvasFun()
        // canvas1()
    },[props.data])
    const array=[]
    props.data.forEach((data,i)=>{
        const color=(data.resource_name==='training' && 'rgb(32, 65, 149)') ||
                    (data.resource_name==='test'&&'rgb(165, 39, 20)')||
                    (data.resource_name==='mentoring'&&'rgb(242, 166, 0)')||
                    (data.resource_name==='coaching'&&'rgb(15, 157, 88)')||
                    (data.resource_name==='interviews'&&'rgb(171, 71, 188)')||
                    (data.resource_name==='interview'&&'rgb(0, 172, 193)')
        return(array.push({
            backgroundColor:color,
            borderColor:color,
            fill: false,
            borderWidth : 15,
            pointRadius : 0,
            data: [
                {
                    t: new Date(data.start_date),
                    y: i
                }, {
                    t: new Date(data.end_date),
                    y: i
                }
            ]
        })
        )
    })

    const canvasFun=()=>{
      var ctx1 = document.getElementById("canvas").getContext('2d');
      var scatterChart = new Chart(ctx1, {
              type: 'line',
              data: {
                  datasets: 
                  array.map(data=>
                      ({
                          backgroundColor:data.backgroundColor,
                          borderColor: data.borderColor,
                          fill:data.fill,
                          borderWidth:data.borderWidth,
                          pointRadius:data.pointRadius,
                          data:data.data
                      })
                  )
              },
              options: {
                  legend : {
                      display : false
                  },
                  scales: {
                      xAxes: [{
                          type: 'time',
                          time: {
                              parser: 'MM/DD/YYYY HH:mm',
                              tooltipFormat: 'll HH:mm',
                              unit: 'day',
                              unitStepSize: 1,
                              displayFormats: {
                                'day': 'MM/DD/YYYY'
                              }
                            }
                      }],
                      yAxes : [{
                          type: 'linear',
                          ticks : {
                              beginAtZero :true,
                              min:0,
                              max : props.data.length,
                              stepSize:1
                          }
                      }]
                  }
              }
          });
    }
    return (
        <div className="marginBottom">
            <p className="heading">CEP Overview</p>
            <div className='box2 '>
                <canvas id="canvas" style={{height:'300px',width:'100%'}}></canvas>
            <div className="resourceNameMain flexSpaceBetween">
                {resource_name.map((data,i) =>{
                    return(
                    <div className="flex" key={i}>
                        <div className="skillBox" style={{background:colors[i]}}></div>
                        <p>{data}</p>
                    </div>
                )})}
            </div>
            </div>
        </div>
    )
}
