import React from 'react'
import Box from '../ReusebleComponent/Box'
import { Pie } from 'react-chartjs-2'

export default function Skills(props) {
    const colors=['#FF5733','#FFA833','#52FF33','#33C1FF']
    const datasets=[{
        data:props.data.map(data =>data.level),
        backgroundColor:colors
    }]
    return (
       <Box heading="Skills Covered">
            <p className="avgText">Skills covered in training</p>
            <div className="pieChart">
                <Pie
                    data={{
                        // labels:'',
                        datasets:datasets
                    }}
                    options= {{
                        tooltips: {
                             enabled: false
                        },
                        maintainAspectRatio: false
                    }}
                />
            </div>
            <div>
                {props.data.map((data,i)=>(
                    <div className="flex" style={{marginBottom:'5px'}} key={data.id}>
                        <div className="skillBox" style={{background:colors[i]}} ></div>
                        <p className='skill'>{data.skill}</p>
                    </div>
                ))}
            </div>
        </Box>
    )
}
