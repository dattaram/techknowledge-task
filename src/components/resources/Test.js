import React, { useState, useEffect } from 'react'
import Data from '../../cep_test.json'

export default function Test() {
    const [test,setTest]=useState([])

    useEffect(()=>{
        setTest(Data.cep_plan)
    },[])

    return (
        <div className="page">
            <p className="heading">Test</p>

            <div className='flexWrap'>
            {test.map((data,i)=>(
                <div key={i}>
                    {data.cep_resources.tests.resources.length>0 &&
                    <div className="resourcesBox">{data.cep_resources.tests.resources.map(data=>(
                        data.resource_id
                    ))}</div>}
                </div>
            ))}
            </div>
        </div>
    )
}
