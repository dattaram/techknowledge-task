import React, { useState, useEffect } from 'react'
import Data from '../../cep_test.json'

export default function Interview() {
    const [interview,setInterview]=useState([])

    useEffect(()=>{
        setInterview(Data.cep_plan)
    },[])
    console.log(interview)
    return (
        <div className="page">
            <p className="heading">Interview</p>

            <div className='flexWrap'>
            {interview.map((data,i)=>(
                <div key={i}>
                    {data.cep_resources.interviews.resources.length>0 &&
                    <div className="resourcesBox">{data.cep_resources.interviews.resources.map(data=>(
                        data.resource_id
                    ))}</div>}
                </div>
            ))}
            </div>
        </div>
    )
}
