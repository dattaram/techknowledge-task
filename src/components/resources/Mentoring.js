import React, { useState, useEffect } from 'react'
import Data from '../../cep_test.json'

export default function Mentoring() {
    const [mentoring,setMentoring]=useState([])

    useEffect(()=>{
        setMentoring(Data.cep_plan)
    },[])

    return (
        <div className="page">
            <p className="heading">Mentoring Sessions</p>

            <div className='flexWrap'>
            {mentoring.map((data,i)=>(
                <div key={i}>
                    {data.cep_resources.mentoring_sessions.resources.length>0 &&
                    <div className="resourcesBox">{data.cep_resources.mentoring_sessions.resources.map(data=>(
                        data.resource_id
                    ))}</div>}
                </div>
            ))}
            </div>
        </div>
    )
}
