import React, { useState, useEffect } from 'react'
import Data from '../../cep_test.json'

export default function Training() {
    const [training,setTraining]=useState([])

    useEffect(()=>{
        setTraining(Data.cep_plan)
    },[])

    return (
        <div className="page">
            <p className="heading">Training</p>

            <div className='flexWrap'>
            {training.map((data,i)=>(
                <div key={i}>
                    {data.cep_resources.trainings.resources.length>0 &&
                    <div className="resourcesBox">{data.cep_resources.trainings.resources.map(data=>(
                        data.resource_id
                    ))}</div>}
                </div>
            ))}
            </div>
        </div>
    )
}
