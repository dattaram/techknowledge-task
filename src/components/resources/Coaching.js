import React, { useState, useEffect } from 'react'
import Data from '../../cep_test.json'

export default function Coaching() {
    const [coachings,setCoaching]=useState([])

    useEffect(()=>{
        setCoaching(Data.cep_plan)
    },[])

    return (
        <div className="page">
            <p className="heading">Coaching Sessions</p>

            <div className='flexWrap'>
            {coachings.map((data,i)=>(
                <div key={i}>
                    {data.cep_resources.coaching_sessions.resources.length>0 &&
                    <div className="resourcesBox">{data.cep_resources.coaching_sessions.resources.map(data=>(
                        data.resource_id
                    ))}</div>}
                </div>
            ))}
            </div>
        </div>
    )
}
