import React from 'react'
import { NavLink } from 'react-router-dom'

export default function SideNavbar(props) {
    return (
        <div className="sideBarMain">
            <div className="activePage">
                <p></p>
            </div>
            <div className="sideNavItems">
                <NavLink to="/" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Overview</p>
                </NavLink>
                <NavLink to="/trainings" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Trainings</p>
                </NavLink>
                <NavLink to="/interview" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Interviews</p>
                </NavLink>
                <NavLink to="/test" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Test</p>
                </NavLink>
                <NavLink to="/mentoring" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Mentoring Sessions</p>
                </NavLink>
                <NavLink to="/coaching" activeClassName="active" className="sideNavItem">
                    <p className="centerAlign">Coaching Sessions</p>
                </NavLink>
            </div>
        </div>
    )
}
