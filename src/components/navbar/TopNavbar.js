import React, { useEffect, useState } from 'react'
import {MdNotifications} from 'react-icons/md'
import Data from '../../cep_test.json'
import { FaUser } from "react-icons/fa";

export default function TopNavbar() {
    const [name,setName]=useState({})
    useEffect(()=>{
        setName(Data.coach_details)
    },[])
    return (
        <div className="topBarMain">
            <div className="flexSpaceBetween">
                <div className="logoMain">
                    <p className="logo">MyCareerNext</p>
                </div>
                <div className="flexSpaceBetween topNavItems">
                    <p>learn</p>
                    <p>Network</p>
                    <p>Evaluate</p>
                    <p>Jobs</p>
                    <div style={{borderRight:'1px solid white',borderLeft:'1px solid white',height:'50%',padding:'0 10px'}}>
                        <MdNotifications />
                    </div>
                    <div className="flexSpaceBetween"> 
                        <div className="userImg flex" style={{justifyContent:'center',color:'black'}}>
                            <FaUser />
                        </div>
                        <p>{name.first_name}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
